# -*- coding: utf-8 -*-
{
    'name': "lote_atributos",

    'summary': """
        - Factura: aparece nº lote y atributos del producto
        - Albaran: aparece los atributos del producto
        - Tambien se agrega configuración para que de por válidos los nif
        - Incluye campo de registro sanitario y ovalo para las facturas
        - Prueba de git""",

    'description': """
	Con este modulo cambiamos la factura para que salga el numero de lote y los atributos del producto
    """,

    'author': "Cooffee",
    'website': "http://www.cooffee.es",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'stock' , 'base_vat'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/factura.xml',
        'views/albaran.xml',
        'views/registroSanitario.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
