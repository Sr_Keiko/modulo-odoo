# -*- coding: utf-8 -*-

from odoo import models, fields, api

class InvoiceLines(models.Model):
    _inherit = "account.invoice.line"
    stock_move_lines = fields.One2many(string="Líneas de albarán", comodel_name="stock.move.line", compute="_get_move_lines")
    @api.one
    def _get_move_lines(self):
        resulting_move_ids = []
        for sale_line in self.sale_line_ids:
            for stock_move in sale_line.move_ids:# No son lineas, son moves
                for move_line_id in stock_move.move_line_ids:
                    resulting_move_ids.append( move_line_id.id )
        self.stock_move_lines = resulting_move_ids

class VatEs(models.Model):
    _inherit = "res.partner"

    def check_vat_es(self, vat):
        return True

class ResCompanyInherit(models.Model):
    _inherit = "res.company"

    registro_sanitario = fields.Char(string='Registro Sanitario')